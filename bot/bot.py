from aiogram import Bot, Dispatcher

from handlers import register_all_routes
from db import register_db

#from config import cfg


def main():
    bot = Bot('token', parse_mode="HTML")
    dp = Dispatcher()

    register_all_routes(dp)

    dp.run_polling(bot)

if __name__ == "__main__":
    main()