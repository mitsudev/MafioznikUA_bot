from dataclasses import dataclass
from dotenv import dotenv_values

@dataclass
class Config:
    TG_BOT_TOKEN: str

def load_config(path: str = None):
    env = dotenv_values()
    return Config(**env)
