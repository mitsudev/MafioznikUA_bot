from sqlalchemy import (
    BigInteger,Text,
    Column,ForeignKey
)

from db.base import Base

class GameRoom(Base):
    __tablename__ = 'games'
    id = Column(BigInteger, primary_key=True)

class Player(Base):
    __tablename__ = 'players'
    user_id = Column(BigInteger)
    room_id = Column(BigInteger,ForeignKey('games.id'))
