from aiogram.dispatcher.router import Router
from aiogram.types import Message

router = Router()

@router.message(commands=["start",'help'])
async def command_start_handler(message: Message) -> None:
    await message.answer(f"Hello, <b>{message.from_user.full_name}!</b>")
