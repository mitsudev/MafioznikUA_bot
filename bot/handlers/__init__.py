from aiogram import Dispatcher

from .base import router as base_router
from .game import router as game_router


def register_all_routes(dp: Dispatcher) -> None:
    dp.include_router(base_router)
    dp.include_router(game_router)
