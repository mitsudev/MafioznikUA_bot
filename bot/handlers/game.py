from aiogram.dispatcher.router import Router

from aiogram.types import CallbackQuery, ChatMemberUpdated, Message
from aiogram.types.inline_keyboard_button import InlineKeyboardButton
from aiogram.types.inline_keyboard_markup import InlineKeyboardMarkup

from aiogram.dispatcher.filters.callback_data import CallbackData

class GameStartAction(CallbackData, prefix="play"):
    pass


router = Router()

@router.message(commands=["game"])
async def command_start_game_handler(message: Message) -> None:

    await message.answer(
        "Набір учасників в гру:",
        reply_markup=InlineKeyboardMarkup(
            inline_keyboard=[[InlineKeyboardButton(text="Приєднатись до гри",callback_data=GameStartAction().pack())]]
            #inline_keyboard=[[InlineKeyboardButton(text="Приєднатись до гри",url='https://t.me'))]]
        ),
    )


@router.callback_query(GameStartAction.filter())
async def callback_start_game(query: CallbackQuery,callback_data: GameStartAction) -> None:
    await query.answer('Гравця додано')

